let cube = document.querySelector('.cube');
let currentClass = '';
let btn = document.querySelector('.random-button');

function changeSide(index) {
    let side;
    switch(index){
        case 1 :
            side = 'front';
            break;
        case 2 :
            side = 'top';
            break;
        case 3 :
            side = 'left';
            break;
        case 4 :
            side = 'bottom';
            break;
        case 5 :
            side = 'right';
            break;
        case 6 :
            side = 'back';
            break;
    }
    let showClass = 'show-' + side;
    if (currentClass) {
        cube.classList.remove(currentClass);
    }
    cube.classList.add(showClass);
    currentClass = showClass;
}

function setRandom(event) {
    event.preventDefault();
    let random = Math.floor((Math.random() * 6) + 1);
    changeSide(random).click();
}

btn.addEventListener('click', setRandom);
