navigator.geolocation.getCurrentPosition(function(location) {
    let latitudeCurrent = location.coords.latitude;
    let longitudeCurrent = location.coords.longitude;
    let latitudeNice = 43.700936;
    let longitudeNice = 7.268391;
    let latitudeMarseille = 43.2969500;
    let longitudeMarseille = 5.3810700;

    let triangleBermudes = [
        [26.30236, -79.717972],
        [18.412005, -64.546649],
        [32.292242, -64.799874],
    ];

    let markers = [
        ["Current Location",latitudeCurrent,longitudeCurrent],
        ["Nice, France",latitudeNice,longitudeNice]
    ];

    let map = L.map('mapid').setView([latitudeCurrent, longitudeCurrent], 13);

    L.tileLayer('http://tile.stamen.com/toner/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    for (let i = 0; i < markers.length; i++) {
        marker = new L.marker([markers[i][1],markers[i][2]])
            .bindPopup(markers[i][0])
            .addTo(map);
    }

    L.polygon(triangleBermudes, { color: 'red' }).addTo(map);

    L.circle([latitudeCurrent,longitudeCurrent], {
        color: 'blue',
        fillColor: '#00f',
        fillOpacity: 0.5,
        radius: 20
    }).addTo(map);

    let distance = calcCrow(latitudeNice, longitudeNice, latitudeMarseille, longitudeMarseille);

    console.log(`La distance entre Nice et Marseille (à vol d'oiseau) est d'environ ${distance} km`);

    function calcCrow(lat1, lon1, lat2, lon2) {
        let R = 6371;
        let dLat = toRad(lat2 - lat1);
        let dLon = toRad(lon2 - lon1);
        lat1 = toRad(lat1);
        lat2 = toRad(lat2);

        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
        return d;
    }

    function toRad(value) {
        return value * Math.PI / 180;
    }
});