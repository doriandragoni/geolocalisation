navigator.geolocation.getCurrentPosition(function(location) {
    let latitudeCurrent = location.coords.latitude;
    let longitudeCurrent = location.coords.longitude;
    let latitudeNice = 43.700936;
    let longitudeNice = 7.268391;

    let markers = [
        ["Current Location",latitudeCurrent,longitudeCurrent],
        ["Nice, France",latitudeNice,longitudeNice]
    ];

    let map = L.map('mapid').setView([latitudeCurrent, longitudeCurrent], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    for (let i = 0; i < markers.length; i++) {
        marker = new L.marker([markers[i][1],markers[i][2]])
            .bindPopup(markers[i][0])
            .addTo(map);
    }
});