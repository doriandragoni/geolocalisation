navigator.geolocation.getCurrentPosition(function(location) {
    let latitudeCurrent = location.coords.latitude;
    let longitudeCurrent = location.coords.longitude;
    let latitudeNice = 43.700936;
    let longitudeNice = 7.268391;
    let triangleBermudes = [
        [26.30236, -79.717972],
        [18.412005, -64.546649],
        [32.292242, -64.799874],
    ];

    let markers = [
        ["Current Location",latitudeCurrent,longitudeCurrent],
        ["Nice, France",latitudeNice,longitudeNice]
    ];

    let map = L.map('mapid').setView([latitudeCurrent, longitudeCurrent], 13);

    L.tileLayer('http://tile.stamen.com/toner/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    for (let i = 0; i < markers.length; i++) {
        marker = new L.marker([markers[i][1],markers[i][2]])
            .bindPopup(markers[i][0])
            .addTo(map);
    }

    L.polygon(triangleBermudes, { color: 'red' }).addTo(map);

    L.circle([latitudeCurrent,longitudeCurrent], {
        color: 'blue',
        fillColor: '#00f',
        fillOpacity: 0.5,
        radius: 20
    }).addTo(map);
});