//DeviceOrientation
let alpha = document.getElementById("alpha");
let beta = document.getElementById("beta");
let gamma = document.getElementById("gamma");

//DeviceMotion
let  accelerationX = document.getElementById("acceleration-x");
let  accelerationY = document.getElementById("acceleration-y");
let  accelerationZ = document.getElementById("acceleration-z");
let  accelerationGravityX = document.getElementById("acceleration-gravity-x");
let  accelerationGravityY = document.getElementById("acceleration-gravity-y");
let  accelerationGravityZ = document.getElementById("acceleration-gravity-z");
let  rotationX = document.getElementById("rotation-x");
let  rotationY = document.getElementById("rotation-y");
let  rotationZ = document.getElementById("rotation-z");

if (window.DeviceOrientationEvent) {
    window.addEventListener('deviceorientation', (event) => {
        alpha.innerText = event.alpha;
        beta.innerText = event.beta;
        gamma.innerText = event.gamma;
    });
}

if (window.DeviceOrientationEvent) {
    window.addEventListener('devicemotion', (event) => {
        accelerationX.innerText = event.acceleration.x;
        accelerationY.innerText = event.acceleration.y;
        accelerationZ.innerText = event.acceleration.z;
        accelerationGravityX.innerText = event.accelerationIncludingGravity.x;
        accelerationGravityY.innerText = event.accelerationIncludingGravity.y;
        accelerationGravityZ.innerText = event.accelerationIncludingGravity.z;
        rotationX.innerText = event.rotationRate.alpha;
        rotationY.innerText = event.rotationRate.beta;
        rotationZ.innerText = event.rotationRate.gamma;
    });
}