let labelTouchEvent = document.getElementById("labelTouchEvent");

window.addEventListener('touchstart', () => {
    labelTouchEvent.innerText = "Touch Start"
});

window.addEventListener('touchend', () => {
    labelTouchEvent.innerText = "Touch End"
});

window.addEventListener('touchmove', () => {
    labelTouchEvent.innerText = "Touch Move"
});