let canvas = document.getElementById('canvas-house');
let canvasDirection = document.getElementById('canvas-arrow');

function draw() {
    let ctx = canvas.getContext('2d');

    ctx.fillStyle = 'green';
    ctx.fillRect(0, 100, 500, 400);

    ctx.fillStyle = 'lightblue';
    ctx.fillRect(0, 0, 500, 100);

    ctx.fillStyle = 'yellow';
    ctx.beginPath();
    ctx.arc(450, 50, 40, 0, Math.PI * 2);
    ctx.fill();

    ctx.fillStyle = 'orange';
    ctx.fillRect(100, 150, 200, 120);

    ctx.fillStyle = 'brown';
    ctx.beginPath();
    ctx.moveTo(200, 150);
    ctx.lineTo(100, 150);
    ctx.lineTo(200, 80);
    ctx.lineTo(300, 150);
    ctx.fill();
}

draw();

function drawArrow(angle) {
    let ctx = canvasDirection.getContext('2d');

    ctx.clearRect(0, 0, 100, 100);

    ctx.fillStyle = 'black';
    ctx.moveTo(50, 60);
    ctx.lineTo(30, 70);
    ctx.lineTo(50, 30);
    ctx.lineTo(70, 70);
    ctx.fill();
}

if (window.DeviceOrientationEvent) {
    window.addEventListener('deviceorientation', (event) => {
        drawArrow(event.alpha);
    });
}
