//DeviceOrientation
let alpha = document.getElementById("alpha");
let beta = document.getElementById("beta");
let gamma = document.getElementById("gamma");

if (window.DeviceOrientationEvent) {
    window.addEventListener('deviceorientation', (event) => {
        alpha.innerText = event.alpha;
        beta.innerText = event.beta;
        gamma.innerText = event.gamma;
    });
}

//Geolocation
let lat = document.getElementById('lat');
let lng = document.getElementById('lng');

navigator.geolocation.getCurrentPosition(function(location) {
    let latitudeCurrent = location.coords.latitude;
    let longitudeCurrent = location.coords.longitude;

    lat.innerText = latitudeCurrent.toString();
    lng.innerText = longitudeCurrent.toString();
});
